#ifndef APOAUTOMAT_DEVICE_INIT_H
#define APOAUTOMAT_DEVICE_INIT_H

/*
 * Funkce pro nalezeni a inicializaci PCI karty.
 */

#include "types.h"


/**
 * Projde adresare v devdir ("/sys/bus/pci/devices") a pro kazdy adresar
 * zavola try_device().
 * V pripade chyby ukonci program.
 */
void find_device(struct DeviceInfo *deviceInfo);


/**
 * echo 1 > /.../enable
 */
void enable_device(struct DeviceInfo *deviceInfo);


/**
 * Namapuje pamet PCI zarizeni do pameti procesu.
 */
void mmap_device(const struct DeviceInfo *deviceInfo,
                 struct DeviceState *ds);


#endif

