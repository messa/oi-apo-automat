#ifndef APOAUTOMAT_COMPUTE_RETURNS_H
#define APOAUTOMAT_COMPUTE_RETURNS_H

#include "coin_stack.h"
#include "types.h"


int compute_return(int value,
                   const struct Coin *coinTypes,
                   const struct CoinStack *coinStack,
                   int *returnCoinCounts);



#endif
