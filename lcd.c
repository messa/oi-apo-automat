
#include "lcd.h"

#include "chmod_lcd.h"
#include "kbd_hw.h"

#include "ctrl.h"
#include "types.h"


void lcd_set_cursor(struct DeviceState *ds, int line, int pos) {
    ctrl_write_data(ds, BUS_LCD_INST_o, CHMOD_LCD_POS + pos + line * 0x40);
}



void write_lcd_line(struct DeviceState *ds,
                    char *cache,
                    int line,
                    const char *what)
{
    int i;
    int behind = 0;
    for (i = 0; i < 16; i++) {
        if (what[i] == '\0') {
            behind = 1;
        }
        if (behind) {
            if (cache[i] != ' ') {
                cache[i] = ' ';
                lcd_set_cursor(ds, line, i);
                ctrl_write_data(ds, BUS_LCD_WDATA_o, cache[i]);
            }
        } else {
            if (cache[i] != what[i]) {
                cache[i] = what[i];
                lcd_set_cursor(ds, line, i);
                ctrl_write_data(ds, BUS_LCD_WDATA_o, cache[i]);
            }
        }
    }
}


/**
 * Napise text na LCD displej.
 */
void write_lcd(struct DeviceState *ds,
               const char *line1,
               const char *line2)
{
    write_lcd_line(ds, ds->lcdLineCache1, 0, line1);
    write_lcd_line(ds, ds->lcdLineCache2, 1, line2);
#if 0
    int i;
    for (i = 0; i < strlen(line1) && i < 16; i++) {
        lcd_set_cursor(ds, 0, i);
        ctrl_write_data(ds, BUS_LCD_WDATA_o, line1[i]);
    }
    for (i = 0; i < strlen(line2) && i < 16; i++) {
        lcd_set_cursor(ds, 1, i);
        ctrl_write_data(ds, BUS_LCD_WDATA_o, line2[i]);
    }
#endif
}


void lcd_force_clean(struct DeviceState *ds) {
    int i;
    write_lcd(ds, "", "");
    for (i = 0; i < 16; i++) {
        lcd_set_cursor(ds, 0, i);
        ctrl_write_data(ds, BUS_LCD_WDATA_o, ' ');
        lcd_set_cursor(ds, 1, i);
        ctrl_write_data(ds, BUS_LCD_WDATA_o, ' ');
    }
}


/**
 * Inicializace LCD displeje na bazmeku.
 */
void lcd_initialize(struct DeviceState *ds) {
    int i;
    for (i = 0; i < 16; i++) {
        ds->lcdLineCache1[i] = ' ';
        ds->lcdLineCache2[i] = ' ';
    }

    ctrl_write_data(ds, BUS_LCD_INST_o, CHMOD_LCD_MOD);
    wait_a_little();
    ctrl_write_data(ds, BUS_LCD_INST_o, CHMOD_LCD_MOD);
    ctrl_write_data(ds, BUS_LCD_INST_o, CHMOD_LCD_CLR);
    ctrl_write_data(ds, BUS_LCD_INST_o, CHMOD_LCD_DON);
    for (;;) {
        uint8_t value = ctrl_read_data(ds, BUS_LCD_STAT_o);
        if (value == CHMOD_LCD_BF) {
            continue;
        }
        break;
    }
}


