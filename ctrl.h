#ifndef APOAUTOMAT_CTRL_H
#define APOAUTOMAT_CTRL_H

#include "types.h"


void wait_a_little();

void ctrl_enable_power(struct DeviceState *ds);
void ctrl_disable_power(struct DeviceState *ds);

void data_out(struct DeviceState *ds, uint8_t data);

void ctrl_write_data(struct DeviceState *ds, uint8_t ctrlAddress, uint8_t data);

uint8_t ctrl_read_data(struct DeviceState *ds, uint8_t ctrlAddress);

void piezo_beep_start(struct DeviceState *ds);

void piezo_beep_stop(struct DeviceState *ds);


#endif

