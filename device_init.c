
#include "device_init.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "constants.h"
#include "types.h"
#include "util.h"


/**
 * Otevre config soubor PCI zarizeni a pokud jeho vendor a device id
 * konstantam, nastavi prislusne atributy struktury deviceInfo.
 */
void try_device(const char *devicePath,
                struct DeviceInfo *deviceInfo)
{
    char contents[0xffff];
    char path[500];

    snprintf(path, sizeof(path), "%s/config", devicePath);
    read_file(contents, sizeof(contents), path);
    if (*((uint16_t*) contents) != APO_DEVICE_VENDOR_ID) {
        return;
    }
    if (*((uint16_t*) (contents+2)) != APO_DEVICE_ID) {
        return;
    }
    deviceInfo->bar0 = *((uint32_t*) (contents+0x10));
    strncpy(deviceInfo->path, devicePath, sizeof(deviceInfo->path));
    printf("yes! bar0: %x path: %s\n", (unsigned) deviceInfo->bar0, deviceInfo->path);
}


/**
 * Projde adresare v "/sys/bus/pci/devices" a pro kazdy adresar
 * zavola try_device().
 * V pripade chyby ukonci program.
 */
void find_device(struct DeviceInfo *deviceInfo) {
    DIR *d = opendir(SYS_DEVICES_DIR);
    if (d == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }
    for (;;) {
        struct dirent *entry;
        errno = 0;
        entry = readdir(d);
        if (entry == NULL) {
            if (errno == 0) {
                /* end of the directory stream reached */
                break;
            } else {
                perror("readdir");
                exit(EXIT_FAILURE);
            }
        }
        if (entry->d_name[0] == '.') {
            /* nechceme pseudoadresare".", ".." */
            continue;
        }
        {
            char fullPath[500];
            snprintf(fullPath, 500, "%s/%s", SYS_DEVICES_DIR, entry->d_name);
            printf("Trying %s (%s)...\n", entry->d_name, fullPath);
            try_device(fullPath, deviceInfo);
        }
    }
    if (closedir(d) != 0) {
        perror("closedir");
        exit(EXIT_FAILURE);
    }
}


/**
 * echo 1 > /.../enable
 */
void enable_device(struct DeviceInfo *deviceInfo) {
    int fd;
    char path[500];
    snprintf(path, sizeof(path), "%s/enable", deviceInfo->path);

    /* otevreni souboru /proc/.../enable */
    fd = open(path, O_WRONLY);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    /* zapsani "1" do /proc/.../enable */
    {
        ssize_t s = write(fd, "1", 1);
        if (s == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }
        if (s != 1) {
            fprintf(stderr, "written %d bytes instead of 1\n", (int) s);
            exit(EXIT_FAILURE);
        }
    }

    /* zavreni souboru /proc/.../enable */
    {
        int r = close(fd);
        if (r == -1) {
            perror("close");
            exit(EXIT_FAILURE);
        }
    }
    printf("Written 1 to %s\n", path);
}


/**
 * Namapuje pamet PCI zarizeni do pameti procesu.
 */
void mmap_device(const struct DeviceInfo *deviceInfo,
                 struct DeviceState *ds)
{
    int fd;
    volatile void *m = NULL;
    /* otevreni /dev/mem */
    fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    m = mmap(NULL, 65536, PROT_READ|PROT_WRITE, MAP_SHARED, fd, deviceInfo->bar0);
    if (m == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }
    printf("mmap(..., %x) = %x\n", (unsigned) deviceInfo->bar0, (unsigned) m);

    /* zavrit /dev/mem - nepotrebujeme to mit otevrene, mame to namapovane */
    {
        int r = close(fd);
        if (r == -1) {
            perror("close");
            exit(EXIT_FAILURE);
        }
    }

    ds->ctrl = ((unsigned char *) m) + 0x8060;
    ds->dataIn = ((unsigned char *) m) + 0x8040;
    ds->dataOut = ((unsigned char *) m) + 0x8020;
}

