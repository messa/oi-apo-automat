#ifndef APOAUTOMAT_MAIN_LOOP_H
#define APOAUTOMAT_MAIN_LOOP_H

#include "coin_stack.h"
#include "types.h"


/**
 * Hlavni smycka automatu na vydej jizdenek.
 */
void main_loop(struct DeviceState *ds, struct CoinStack *coinStack);


#endif

