#ifndef APOAUTOMAT_UTIL_H
#define APOAUTOMAT_UTIL_H

#include <stdio.h>

/*
 * Ruzne pomocne funkce.
 */


/**
 * Nacte obsah souboru do pameti.
 * V pripade chyby ukonci program.
 */
void read_file(char *buffer,
               size_t bufferSize,
               const char *path);


#endif

