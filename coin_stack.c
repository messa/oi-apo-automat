
#include "coin_stack.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "constants.h"


void coin_stack_init(struct CoinStack *cs,
                     const char *filename)
{
    memset(cs, 0, sizeof(*cs));
    cs->filename = filename;
}


void coin_stack_load(struct CoinStack *cs) {
    FILE *f = NULL;
    int i = 0;

    if (cs->filename == NULL) {
        fprintf(stderr, "filename is NULL\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0; !coins_end(coins + i); i++) {
        if (sizeof(cs->coinCounts) / sizeof(cs->coinCounts[0]) < i) {
            fprintf(stderr, "coinCounts array is too small\n");
            exit(EXIT_FAILURE);
        }
        cs->coinCounts[i] = 0;
    }

    f = fopen(cs->filename, "r");
    if (f == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    i = 0;
    for (;;) {
        int coinValue = 0;
        int coinCount = 0;
        char buffer[1024];

        if (fgets(buffer, sizeof(buffer), f) == NULL) {
            if (feof(f)) {
                break;
            }
            perror("fgets");
            exit(EXIT_FAILURE);
        }
        if (buffer[0] == '#') {
            continue;
        }
        if (sscanf(buffer, "%d%d", &coinValue, &coinCount) != 2) {
            fprintf(stderr, "failed to parse line '%s' {%s:%d}\n",
                buffer, __FILE__, __LINE__);
            exit(EXIT_FAILURE);
        }
        if (coins_end(coins + i)) {
            fprintf(stderr, "end of coin type array reached {%s:%d}\n",
                __FILE__, __LINE__);
            exit(EXIT_FAILURE);
        }
        if ((coins + i)->value != coinValue) {
            fprintf(stderr, "coin value not matched {%s:%d}\n",
                __FILE__, __LINE__);
            exit(EXIT_FAILURE);
        }
        if (coinValue < 0) {
            fprintf(stderr, "coin value cannot be negative {%s:%d}\n",
                __FILE__, __LINE__);
            exit(EXIT_FAILURE);
        }

        cs->coinCounts[i] = coinCount;
        i++;
    }

    if (fclose(f) != 0) {
        perror("fclose");
        exit(EXIT_FAILURE);
    }
}


void coin_stack_save(struct CoinStack *cs) {
    FILE *f = NULL;
    int i = 0;

    printf("Ukladam pocty minci do souboru %s\n", cs->filename);

    if (cs->filename == NULL) {
        fprintf(stderr, "filename is NULL\n");
        exit(EXIT_FAILURE);
    }

    f = fopen(cs->filename, "w");
    if (f == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    fprintf(f, "# <value> <count>\n");

    for (i = 0; !coins_end(coins + i); i++) {
        const struct Coin *ct = coins + i;

        if (sizeof(cs->coinCounts) / sizeof(cs->coinCounts[0]) < i) {
            fprintf(stderr, "coinCounts array is too small\n");
            exit(EXIT_FAILURE);
        }

        fprintf(f, "%d %d\n", ct->value, cs->coinCounts[i]);
    }

    fprintf(f, "# end\n");

    if (fclose(f) != 0) {
        perror("fclose");
        exit(EXIT_FAILURE);
    }
}

