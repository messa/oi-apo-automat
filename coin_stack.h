#ifndef APOAUTOMAT_COIN_STACK_H
#define APOAUTOMAT_COIN_STACK_H


/**
 * Zasobnik minci
 */
struct CoinStack {
    const char *filename;
    int coinCounts[6];
};


void coin_stack_init(struct CoinStack *cs, const char *filename);


void coin_stack_load(struct CoinStack *cs);


void coin_stack_save(struct CoinStack *cs);


#endif
