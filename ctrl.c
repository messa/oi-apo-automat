
#include "ctrl.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "chmod_lcd.h"
#include "kbd_hw.h"

#include "types.h"


void wait_a_little() {
    usleep(10000);
}


/*
    8-bitový výstupní registr s řídícími signály emulované sběrnice (emul_bus_ctrl)

    Bit     Označení
    signálu     Pin IO
    Modulu     Pin FPGA     Popis
    0     A0     IO11     K8     Adresový bit 0 (LSB)
    1     A1     IO12     N9     Adresový bit 1
    2     CS0     IO15     N10     Potvrzení výběru periferie (aktivní v L)
    3     WR     IO14     K9     Řízení zápisu (aktivní v L)
    4     RD     IO13     L9     Řízení čtení (aktivní v L)
    5
    6
    7     PWR     IO18     L11     Povolení napájení periferie
*/


void ctrl_set(struct DeviceState *ds, uint8_t newValue) {
    ds->ctrlValue = newValue;
    *(ds->ctrl) = newValue;
}

void ctrl_enable_power(struct DeviceState *ds) {
    ctrl_set(ds, 0xFF);
    wait_a_little();
}

void ctrl_disable_power(struct DeviceState *ds) {
    ctrl_set(ds, 0x00);
    wait_a_little();
}

void ctrl_set_address(struct DeviceState *ds, uint8_t address) {
    if (address > 3) {
        fprintf(stderr, "Invalid address");
        exit(EXIT_FAILURE);
    }
    ctrl_set(ds, (ds->ctrlValue & 0xfc) | address);
}

void ctrl_enable_wr(struct DeviceState *ds) {
    ctrl_set(ds, ds->ctrlValue & 0xf7);
}

void ctrl_disable_wr(struct DeviceState *ds) {
    ctrl_set(ds, ds->ctrlValue | 0x08);
}

void ctrl_enable_rd(struct DeviceState *ds) {
    ctrl_set(ds, ds->ctrlValue & 0xef);
}

void ctrl_disable_rd(struct DeviceState *ds) {
    ctrl_set(ds, ds->ctrlValue | 0x10);
}

void ctrl_enable_cs0(struct DeviceState *ds) {
    ctrl_set(ds, ds->ctrlValue & 0xfb);
}

void ctrl_disable_cs0(struct DeviceState *ds) {
    ctrl_set(ds, ds->ctrlValue | 0x04);
}

void data_out(struct DeviceState *ds,
              uint8_t data)
{
    *(ds->dataOut) = data;
}

void ctrl_write(struct DeviceState *ds,
                uint8_t ctrlAddress)
{
    ctrl_set_address(ds, ctrlAddress);
    ctrl_enable_wr(ds);
    ctrl_enable_cs0(ds);
    wait_a_little();
    ctrl_disable_cs0(ds);
    ctrl_disable_wr(ds);
}

void ctrl_write_data(struct DeviceState *ds,
                     uint8_t ctrlAddress,
                     uint8_t data)
{
    data_out(ds, data);
    ctrl_write(ds, ctrlAddress);
}


uint8_t ctrl_read_data(struct DeviceState *ds,
                       uint8_t ctrlAddress)
{
    uint8_t data = 0;
    ctrl_set_address(ds, ctrlAddress);
    ctrl_enable_rd(ds);
    ctrl_enable_cs0(ds);
    wait_a_little();
    data = *(ds->dataIn);
    ctrl_disable_cs0(ds);
    ctrl_disable_rd(ds);
    return data;
}


void piezo_beep_start(struct DeviceState *ds) {
    /*if (ds->beeping) {
        return;
    }*/
    ctrl_write_data(ds, BUS_KBD_WR_o, 0xFF);
    ds->beeping = 1;
}


void piezo_beep_stop(struct DeviceState *ds) {
    /*if (! ds->beeping) {
        return;
    }*/
    ctrl_write_data(ds, BUS_KBD_WR_o, 0x00);
    ds->beeping = 0;
}


