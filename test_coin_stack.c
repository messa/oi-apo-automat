
#include "coin_stack.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char *argv[]) {
    struct CoinStack coinStack;
    struct CoinStack *cs = &coinStack;

    coin_stack_init(cs, "test_data.coin_stack.txt");
    cs->coinCounts[2] = 16;
    coin_stack_save(cs);

    coin_stack_init(cs, "test_data.coin_stack.txt");
    coin_stack_load(cs);
    if (cs->coinCounts[2] != 16) {
        fprintf(stderr, "Test failed {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    unlink("test_data.coin_stack.txt");

    printf("Test OK (%s)\n", __FILE__);
    return 0;
}

