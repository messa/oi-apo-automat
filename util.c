
#include "util.h"

#include <stdio.h>
#include <stdlib.h>


/**
 * Nacte obsah souboru do pameti.
 * V pripade chyby ukonci program.
 */
void read_file(char *buffer,
               size_t bufferSize,
               const char *path)
{
    size_t s;
    FILE *f = fopen(path, "rb");
    if (f == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    s = fread(buffer, 1, bufferSize-1, f);
    buffer[s] = 0; /* vlozime znak konce retezce */
    if (ferror(f)) {
        fprintf(stderr, "ferror");
        exit(EXIT_FAILURE);
    }
    if (! feof(f)) {
        fprintf(stderr, "not feof");
        exit(EXIT_FAILURE);
    }
    if (fclose(f) != 0) {
        perror("fclose");
        exit(EXIT_FAILURE);
    }
}



