
all: check run

check: test_compute_return test_coin_stack
	./test_compute_return
	./test_coin_stack

test_compute_return: test_compute_return.o compute_return.o
	gcc -g -Wall -pedantic -o test_compute_return test_compute_return.o compute_return.o

test_compute_return.o: test_compute_return.c compute_return.h
	gcc -g -Wall -pedantic -c -o test_compute_return.o test_compute_return.c

test_coin_stack: test_coin_stack.o coin_stack.o
	gcc -g -Wall -pedantic -o test_coin_stack test_coin_stack.o coin_stack.o

test_coin_stack.o: test_coin_stack.c coin_stack.h
	gcc -g -Wall -pedantic -c -o test_coin_stack.o test_coin_stack.c


run: main
	sudo ./main

main: coin_stack.o compute_return.o ctrl.o device_init.o keyboard.o lcd.o main.o main_loop.o util.o
	gcc -g -Wall -pedantic -o main coin_stack.o compute_return.o ctrl.o device_init.o keyboard.o lcd.o main.o main_loop.o util.o

coin_stack.o: coin_stack.c coin_stack.h constants.h
	gcc -g -c -Wall -pedantic -o coin_stack.o coin_stack.c

compute_return.o: compute_return.c compute_return.h
	gcc -g -c -Wall -pedantic -o compute_return.o compute_return.c

ctrl.o: ctrl.c ctrl.h types.h
	gcc -g -c -Wall -pedantic -o ctrl.o ctrl.c

device_init.o: device_init.c constants.h device_init.h ctrl.h types.h util.h constants.h
	gcc -g -c -Wall -pedantic -o device_init.o device_init.c

keyboard.o: keyboard.c keyboard.h constants.h ctrl.h types.h
	gcc -g -c -Wall -pedantic -o keyboard.o keyboard.c

lcd.o: lcd.c lcd.h constants.h ctrl.h types.h
	gcc -g -c -Wall -pedantic -o lcd.o lcd.c

main.o: main.c ctrl.h lcd.h types.h
	gcc -g -c -Wall -pedantic -o main.o main.c

main_loop.o: main_loop.c main_loop.h constants.h ctrl.h keyboard.h lcd.h types.h
	gcc -g -c -Wall -pedantic -o main_loop.o main_loop.c

util.o: util.c util.h
	gcc -g -c -Wall -pedantic -o util.o util.c

clean:
	rm -vf *.o main test_coin_stack test_compute_return

