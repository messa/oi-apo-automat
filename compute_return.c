
#include "compute_return.h"

#include <stdlib.h>
#include <stdio.h>

#include "constants.h"
#include "types.h"

/**
 * Typy minci v parametru coinTypes musi byt serazeny od nejmensi.
 */
int compute_return(int value,
                   const struct Coin *coinTypes,
                   const struct CoinStack *coinStack,
                   int *returnCoinCounts)
{
    const struct Coin *ct = NULL;
    int coinTypesCount = 0;
    int i = 0;

    if (value < 0) {
        fprintf(stderr, "value < 0: %d\n", value);
        exit(EXIT_FAILURE);
    }

    /* zjistime pocet typu minci */
    while (! coins_end(coinTypes + coinTypesCount)) {
        coinTypesCount ++;
    }

    /* pujdeme od konce - od minci s nejvyssi hodnotou po nejnizsi */
    for (i = coinTypesCount-1; i >= 0; i--) {
        ct = coinTypes + i;
        returnCoinCounts[i] = 0;

        while (value >= ct->value &&
               coinStack->coinCounts[i] > returnCoinCounts[i])
        {
            value -= ct->value;
            returnCoinCounts[i] ++;
        }
    }

    if (value != 0) {
        /* neni mozne vratit */
        return 0;
    }

    return 1;
}


