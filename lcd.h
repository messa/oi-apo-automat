#ifndef APOAUTOMAT_LCD_H
#define APOAUTOMAT_LCD_H

#include "types.h"


/**
 * Napise text na LCD displej.
 */
void write_lcd(struct DeviceState *ds,
               const char *line1,
               const char *line2);


/**
 * Inicializace LCD displeje na bazmeku.
 */
void lcd_initialize(struct DeviceState *ds);

void lcd_force_clean(struct DeviceState *ds);

#endif

