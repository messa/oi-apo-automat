
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "chmod_lcd.h"
#include "kbd_hw.h"

#include "coin_stack.h"
#include "ctrl.h"
#include "device_init.h"
#include "lcd.h"
#include "main_loop.h"
#include "types.h"
#include "util.h"

struct DeviceState *onExitDs = NULL;

static void on_apo_automat_exit(int sig){
    ctrl_disable_power(onExitDs);
    exit(EXIT_FAILURE);
}


int main(int argc, char *argv[]) {
    struct DeviceInfo deviceInfo;
    struct DeviceState deviceState;
    struct DeviceState *ds = &deviceState;
    struct CoinStack coinStack;
    struct CoinStack *cs = &coinStack;
    memset(&deviceInfo, 0, sizeof(deviceInfo));
    memset(&deviceState, 0, sizeof(deviceState));
    memset(&coinStack, 0, sizeof(coinStack));

    coin_stack_init(cs, "coins.txt");
    coin_stack_load(cs);

    find_device(&deviceInfo);
    enable_device(&deviceInfo);
    mmap_device(&deviceInfo, &deviceState);

    onExitDs = ds;

    signal(SIGABRT, &on_apo_automat_exit);
    signal(SIGTERM, &on_apo_automat_exit);
    signal(SIGINT, &on_apo_automat_exit);

    printf("start\n");

    /* zapnutí napájení */
    data_out(ds, 0x00);
    ctrl_enable_power(ds);

    /* zapneme piezo */
    ctrl_write_data(ds, BUS_KBD_WR_o, 0xFF);

    /* rozsvitime LEDky */
    ctrl_write_data(ds, BUS_LED_WR_o, 0x13);

    /* inicializace LCD */
    lcd_initialize(ds);
    lcd_force_clean(ds);
    write_lcd(ds, "APO LS 2011/12", "Messner&Lukes");

    /* vypneme piezo */
    ctrl_write_data(ds, BUS_KBD_WR_o, 0x00);

    sleep(1);

    /* zhasneme LEDky */
    ctrl_write_data(ds, BUS_LED_WR_o, 0x00);

    main_loop(ds, cs);

    return 0;
}


