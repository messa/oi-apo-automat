
#include "keyboard.h"

#include "kbd_hw.h"

#include "constants.h"
#include "ctrl.h"
#include "types.h"


/*
 * Schema klavesnice bazmeku:
 *
 *     +---+---+---+  +---+
 *     | 1 | 2 | 3 |  | + |
 *     +---+---+---+  +---+
 *     | 4 | 5 | 6 |
 *     +---+---+---+
 *     | 7 | 8 | 9 |
 *     +---+---+---+  +---+
 *     | * | 0 | # |  | - |
 *     +---+---+---+  +---+
 */


/**
 * Vraci stisknutou klavesu.
 * Pokud zadna klavesa stisknuta neni, nebo jich je stisknutych vice najednou,
 * vrati ' '.
 */
char scan_keyboard(struct DeviceState *ds) {
    char pressedKey = ' ';
    uint8_t value = 0;

    /* levy sloupec klaves */
    {
        ctrl_write_data(ds, BUS_KBD_WR_o, 1 << 0 | 1 << 1 | 0 << 2);
        value = ctrl_read_data(ds, BUS_KBD_RD_o);

        switch (value & 0x1f) {
            case 30: pressedKey = '1'; break;
            case 29: pressedKey = '4'; break;
            case 27: pressedKey = '7'; break;
            case 23: pressedKey = '*'; break;
            case 15: pressedKey = '-'; break;
        }
    }

    /* prostredni sloupec klaves */
    {
        ctrl_write_data(ds, BUS_KBD_WR_o, 1 << 0 | 0 << 1 | 1 << 2);
        value = ctrl_read_data(ds, BUS_KBD_RD_o);

        switch (value & 0x1f) {
            case 30: pressedKey = '2'; break;
            case 29: pressedKey = '5'; break;
            case 27: pressedKey = '8'; break;
            case 23: pressedKey = '0'; break;
            case 15: pressedKey = '+'; break;
        }
    }

    /* pravy sloupec klaves */
    {
        ctrl_write_data(ds, BUS_KBD_WR_o, 0 << 0 | 1 << 1 | 1 << 2);
        value = ctrl_read_data(ds, BUS_KBD_RD_o);

        switch (value & 0x1f) {
            case 30: pressedKey = '3'; break;
            case 29: pressedKey = '6'; break;
            case 27: pressedKey = '9'; break;
            case 23: pressedKey = '#'; break;
        }
    }

    return pressedKey;
}


char get_key(struct DeviceState *ds) {
    char pressedKey = scan_keyboard(ds);
    if (pressedKey != ds->lastPressedKey) {
        ds->pressedKeyTaken = 0;
        ds->pressedKeyScanCount = 0;
    }
    ds->lastPressedKey = pressedKey;

    ds->pressedKeyScanCount ++;

    if (ds->pressedKeyScanCount > MIN_SCAN_COUNT && ! ds->pressedKeyTaken) {
        ds->pressedKeyTaken = 1;
        return pressedKey;
    }

    return ' ';
}

