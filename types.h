#ifndef APOAUTOMAT_TYPES_H
#define APOAUTOMAT_TYPES_H

/*
 * Definice typu pouzivanych v programu.
 */

#include <stdint.h>


/**
 * Informace o zarizeni.
 */
struct DeviceInfo {
    char path[500];
    uint64_t bar0;
};


/**
 */
struct DeviceState {

    /* klavesnice */
    char lastPressedKey;
    int pressedKeyScanCount;
    int pressedKeyTaken;

    int beeping;

    /* LCD */
    char lcdLineCache1[16];
    char lcdLineCache2[16];

    /*
     * budeme si pamatovat hodnotu, kterou davame do ctrl registru, protoze
     * ji z toho registru nelze precist zpet
     */
    uint8_t ctrlValue;

    /* ukazatele do namapovane pameti pro zapis do registru zarizeni */
    volatile uint8_t *ctrl;
    volatile uint8_t *dataIn;
    volatile uint8_t *dataOut;
};


/**
 * Listek.
 */
struct Ticket {
    char key;
    const char *name;
    int price;
};


/**
 * Mince.
 */
struct Coin {
    char key;
    int value;
};


#define tickets_end(t)  ((t)->key==0)
#define coins_end(c)    ((c)->key==0)


#endif


