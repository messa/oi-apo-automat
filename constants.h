#ifndef APOAUTOMAT_CONSTANTS_H
#define APOAUTOMAT_CONSTANTS_H

/*
 * Nejake konstanty.
 */

#include "types.h"


#define SYS_DEVICES_DIR "/sys/bus/pci/devices"

/* Identifikace PCI karty */
#define APO_DEVICE_VENDOR_ID ((uint16_t) 0x1172)
#define APO_DEVICE_ID        ((uint16_t) 0x1f32)

#define MIN_SCAN_COUNT 2


static const struct Ticket tickets[] = {
    {'1', "Zakladni plnocenna",  32},
    {'2', "Zakladni zlevnena",   16},
    {'3', "Kratkodoba plnocena", 24},
    {'4', "Kratkodoba zlevnena", 12},
    {'5', "Celodenni plnocena", 110},
    {'6', "Celodenni zlevnena",  55},
    {0, 0, 0}
};


static const struct Coin coins[] = {
    {'1', 1},
    {'2', 2},
    {'3', 5},
    {'4', 10},
    {'5', 20},
    {'6', 50},
    {0, 0}
};


#endif
