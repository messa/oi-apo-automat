
#include "compute_return.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coin_stack.h"
#include "constants.h"
#include "types.h"


int main(int argc, char *argv[]) {
    struct CoinStack coinStack;
    int returnCoinCounts[6];
    int ok;
    memset(&coinStack, 0, sizeof(coinStack));

    ok = compute_return(13, coins, &coinStack, returnCoinCounts);
    if (ok) {
        fprintf(stderr, "Test failed {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    coinStack.coinCounts[2] = 2; /* 2x 5,- */
    coinStack.coinCounts[1] = 1; /* 1x 2,- */
    coinStack.coinCounts[0] = 1; /* 1x 1,- */

    ok = compute_return(13, coins, &coinStack, returnCoinCounts);
    if (!ok) {
        fprintf(stderr, "Test failed: !ok {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    if (returnCoinCounts[2] != 2) {
        fprintf(stderr, "Test failed {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    if (returnCoinCounts[1] != 1) {
        fprintf(stderr, "Test failed {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    if (returnCoinCounts[0] != 1) {
        fprintf(stderr, "Test failed {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }


    coinStack.coinCounts[2] = 2; /* 2x 5,- */
    coinStack.coinCounts[1] = 0; /* 1x 2,- */
    coinStack.coinCounts[0] = 3; /* 1x 1,- */

    ok = compute_return(13, coins, &coinStack, returnCoinCounts);
    if (!ok) {
        fprintf(stderr, "Test failed: !ok {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    if (returnCoinCounts[2] != 2) {
        fprintf(stderr, "Test failed {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    if (returnCoinCounts[1] != 0) {
        fprintf(stderr, "Test failed {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    if (returnCoinCounts[0] != 3) {
        fprintf(stderr, "Test failed {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    printf("Test OK (%s)\n", __FILE__);
    return 0;
}

