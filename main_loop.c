
#include "main_loop.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "coin_stack.h"
#include "compute_return.h"
#include "constants.h"
#include "ctrl.h"
#include "keyboard.h"
#include "lcd.h"
#include "types.h"


void idle_sleep() {
    usleep(500);
}


enum State {
    STATE_START_ORDERING_TICKETS,
    STATE_ORDERING_TICKETS,
    STATE_START_PAYING_MONEY,
    STATE_PAYING_MONEY,
    STATE_RETURNING_MONEY,
    STATE_PRINTING_TICKETS
};


const char* get_state_name(enum State s) {
    switch (s) {
        case STATE_START_ORDERING_TICKETS: return "STATE_START_ORDERING_TICKETS";
        case STATE_ORDERING_TICKETS: return "STATE_ORDERING_TICKETS";
        case STATE_START_PAYING_MONEY: return "STATE_START_PAYING_MONEY";
        case STATE_PAYING_MONEY: return "STATE_PAYING_MONEY";
        case STATE_RETURNING_MONEY: return "STATE_RETURNING_MONEY";
        case STATE_PRINTING_TICKETS: return "STATE_PRINTING_TICKETS";
    }
    return NULL;
}


struct Context {
    struct DeviceState *ds;
    struct CoinStack *coinStack;
    enum State state;
    int ticketCounts[6];
    int totalPrice;
    int coinCounts[6];
    int remaining;
    int toReturn;
};


void context_init(struct Context *cx, struct DeviceState *ds, struct CoinStack *cs) {
    memset(cx, 0, sizeof(*cx));
    cx->ds = ds;
    cx->coinStack = cs;
    cx->state = STATE_START_ORDERING_TICKETS;
}


void context_debug_print(struct Context *cx) {
    const struct Ticket *t;
    const struct Coin *c;
    printf("Context: state=%s totalPrice=%d remaining=%d toReturn=%d\n",
        get_state_name(cx->state), cx->totalPrice, cx->remaining, cx->toReturn);
    printf("  ticketCounts:");
    for (t = tickets; !tickets_end(t); t++) {
        printf("   %dx %d,-", cx->ticketCounts[t - tickets], t->price);
    }
    printf("\n");
    printf("  coinCounts:");
    for (c = coins; !coins_end(c); c++) {
        printf("   %dx %d,-", cx->coinCounts[c - coins], c->value);
    }
    printf("\n");
    printf("\n");
}


void state_start_ordering_tickets(struct Context *cx) {
    const struct Ticket *t = NULL;
    char buffer[32];

    snprintf(buffer, sizeof(buffer), "Celkem: %d,-", cx->totalPrice);
    write_lcd(cx->ds, "Zadejte jizdenky", buffer);
    piezo_beep_stop(cx->ds);

    printf("Stisknutim tlacitek zadejte pocty jednotlivych jizdenek:\n");
    for (t = tickets; !tickets_end(t); t++) {
        printf("  tlacitko: '%c' nazev: %-30s cena: %d,-\n", t->key, t->name, t->price);
    }
    printf("  tlacitko vpravo nahore: prejit k placeni\n");
    printf("  tlacitko vpravo dole: storno\n");
    printf("\n");

    if (cx->totalPrice != 0) {
        printf("Jiz zadane jizdenky:\n");
        for (t = tickets; !tickets_end(t); t++) {
            int n = cx->ticketCounts[t-tickets];
            if (n != 0) {
                printf("  %-30s %dx %d,- (tlacitko: '%c')\n",
                    t->name, n, t->price, t->key);
            }
        }
        printf("Celkem: %d\n", cx->totalPrice);
        printf("\n");
    }

    cx->state = STATE_ORDERING_TICKETS;
}


void state_ordering_tickets(struct Context *cx) {
    const struct Ticket *t;
    const struct Ticket *chosenTicket;
    char pressedKey;

    pressedKey = get_key(cx->ds);
    if (pressedKey == ' ') {
        idle_sleep();
        return;
    }

    if (pressedKey == '+' || pressedKey == '*') {
        /* tlacitko Prejit k placeni */
        piezo_beep_start(cx->ds);
        printf("Prejit k placeni... Celkem: %d,-\n", cx->totalPrice);
        if (cx->totalPrice == 0) {
            printf("Zadne jizdenky nebyly neobjednany.\n");
            cx->state = STATE_START_ORDERING_TICKETS;
            return;
        }

        /* prepnout do stavu placeni (STATE_START_PAYING_MONEY) */
        cx->remaining = cx->totalPrice;
        memset(cx->coinCounts, 0, sizeof(cx->coinCounts));
        cx->state = STATE_START_PAYING_MONEY;
        return;
    }

    if (pressedKey == '-' || pressedKey == '#') {
        /* tlacitko Storno */
        piezo_beep_start(cx->ds);
        printf("Storno...\n");
        cx->totalPrice = 0;
        memset(cx->ticketCounts, 0, sizeof(cx->ticketCounts));
        cx->state = STATE_START_ORDERING_TICKETS;
        return;
    }

    /* zjistit podle stisknute klavesy, jaka jizdenka byla vybrana */

    chosenTicket = NULL;
    for (t = tickets; ! tickets_end(t); t++) {
        if (pressedKey == t->key) {
            chosenTicket = t;
        }
    }

    if (chosenTicket == NULL) {
        printf("Neplatna klavesa\n");
        return;
    }

    /* zapocitat vybranou jizdenku */

    piezo_beep_start(cx->ds);
    printf("Pridavam listek %s za %d,- (tlacitko '%c')\n",
        chosenTicket->name, chosenTicket->price, chosenTicket->key);
    printf("\n");

    cx->ticketCounts[chosenTicket - tickets] ++;
    cx->totalPrice += chosenTicket->price;

    cx->state = STATE_START_ORDERING_TICKETS;
}


void state_start_paying_money(struct Context *cx) {
    const struct Coin *c = NULL;
    char buffer[32];

    snprintf(buffer, sizeof(buffer), "%d,-", cx->remaining);
    write_lcd(cx->ds, "K zaplaceni:", buffer);

    printf("Zbyva zaplatit: %d,-\n", cx->remaining);
    for (c = coins; !coins_end(c); c++) {
        printf("  tlacitko '%c' mince hodnoty %d,-\n", c->key, c->value);
    }
    printf("  tlacitko vpravo dole: storno (vraceni zatim vhozenych penez)\n");
    printf("\n");

    cx->state = STATE_PAYING_MONEY;
}


void state_paying_money(struct Context *cx) {
    const struct Coin *c;
    const struct Coin *chosenCoin;
    char pressedKey;

    pressedKey = get_key(cx->ds);
    if (pressedKey == ' ') {
        idle_sleep();
        return;
    }

    if (pressedKey == '-' || pressedKey == '#') {
        /* tlacitko Storno - vratime vhozene penize */
        piezo_beep_start(cx->ds);
        printf("Storno...\n");

        cx->toReturn = cx->totalPrice - cx->remaining;
        cx->remaining = 0;
        cx->totalPrice = 0;
        memset(cx->ticketCounts, 0, sizeof(cx->ticketCounts));

        /* vhozene mince pridame do zasobniku */
        {
            int i;
            for (i = 0; ! coins_end(coins + i); i++) {
                cx->coinStack->coinCounts[i] += cx->coinCounts[i];
                cx->coinCounts[i] = 0;
            }
        }

        if (cx->toReturn) {
            cx->state = STATE_RETURNING_MONEY;
            return;
        } else {
            cx->state = STATE_START_ORDERING_TICKETS;
            return;
        }
    }

    /* zjistit podle stisknute klavesy, jaka mince byla vybrana (vhozena) */

    chosenCoin = NULL;
    for (c = coins; ! coins_end(c); c++) {
        if (pressedKey == c->key) {
            chosenCoin = c;
        }
    }

    if (chosenCoin == NULL) {
        printf("Neplatna klavesa\n");
        return;
    }

    /* zapocitat vybranou (vhozenou) minci */

    piezo_beep_start(cx->ds);
    printf("Pridavam minci %d,- (tlacitko '%c')\n",
        chosenCoin->value, chosenCoin->key);
    printf("\n");

    cx->coinCounts[chosenCoin - coins] ++;
    cx->remaining -= chosenCoin->value;

    if (cx->remaining <= 0) {
        int returnCoinCounts[6];

        cx->toReturn = - cx->remaining;
        cx->remaining = 0;

        /* vhozene mince pridame do zasobniku */
        {
            int i;
            for (i = 0; ! coins_end(coins + i); i++) {
                cx->coinStack->coinCounts[i] += cx->coinCounts[i];
                cx->coinCounts[i] = 0;
            }
        }


        if (!compute_return(cx->toReturn, coins, cx->coinStack, returnCoinCounts)) {
            /* nemuzeme vratit - zrusime transakci */
            piezo_beep_start(cx->ds);
            write_lcd(cx->ds, "Neni dost minci", "na vraceni");
            piezo_beep_stop(cx->ds);
            sleep(3);

            piezo_beep_start(cx->ds);
            write_lcd(cx->ds, "Transakce je", "zrusena");
            piezo_beep_stop(cx->ds);
            sleep(3);

            cx->toReturn += cx->totalPrice;
            cx->totalPrice = 0;
            memset(cx->ticketCounts, 0, sizeof(cx->ticketCounts));
        }

        cx->state = STATE_RETURNING_MONEY;
        return;
    }

    cx->state = STATE_START_PAYING_MONEY;
}


void state_returning_money(struct Context *cx) {
    const struct Coin *c = NULL;
    int i = 0;
    int returnCoinCounts[6];
    int ok = 0;
    char buffer[32];

    if (cx->toReturn == 0) {
        /* neni nic k vraceni */
        cx->state = STATE_PRINTING_TICKETS;
        return;
    }

    printf("Vraceni castky: %d,-\n", cx->toReturn);

    ok = compute_return(cx->toReturn, coins, cx->coinStack, returnCoinCounts);
    if (! ok) {
        fprintf(stderr, "ERROR: !ok {%s:%d}\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    for (c = coins; ! coins_end(c); c++) {
        int n = returnCoinCounts[c - coins];
        if (n == 0) {
            continue;
        }
        printf("Vraceni %dx %d,-\n", n, c->value);
        piezo_beep_start(cx->ds);
        snprintf(buffer, sizeof(buffer), "%dx %d,-", n, c->value);
        write_lcd(cx->ds, "Vraceni minci:", buffer);
        piezo_beep_stop(cx->ds);

        cx->coinStack->coinCounts[c - coins] -= n;

        if (cx->coinStack->coinCounts[c - coins] < 0) {
            /* jenom pro jistotu - tohle by se nemelo stavat */
            fprintf(stderr,
                "ERROR: pocet minci v zasobniku vysel negativni: "
                "%d {%s:%d}\n",
                cx->coinStack->coinCounts[c - coins],
                __FILE__, __LINE__);
            exit(EXIT_FAILURE);
        }

        /* chvilku pockame, at se to na LCD displeji da precist */
        sleep(3);
    }

    coin_stack_save(cx->coinStack);

    cx->toReturn = 0;
    cx->state = STATE_PRINTING_TICKETS;
}


void state_printing_tickets(struct Context *cx) {
    char buffer[32];
    const struct Ticket *t = NULL;

    if (cx->totalPrice == 0) {
        /* neni nic k vytisknuti */
        cx->state = STATE_START_ORDERING_TICKETS;
        return;
    }

    for (t = tickets; ! tickets_end(t); t++) {
        int n = cx->ticketCounts[t - tickets];
        if (n == 0) {
            continue;
        }

        printf("Tisknu %dx listek '%s' za %d,-\n", n, t->name, t->price);
        snprintf(buffer, sizeof(buffer), "Tisk %dx %d,-", n, t->price);
        write_lcd(cx->ds, buffer, t->name);
        piezo_beep_start(cx->ds);
	usleep(100000);
        piezo_beep_stop(cx->ds);

        /* chvilku pockame, at se to na LCD displeji da precist */
        sleep(3);
    }

    cx->totalPrice = 0;
    memset(cx->ticketCounts, 0, sizeof(cx->ticketCounts));
    cx->state = STATE_START_ORDERING_TICKETS;
}


/**
 * Hlavni smycka automatu na vydej jizdenek.
 */
void main_loop(struct DeviceState *ds, struct CoinStack *coinStack) {
    struct Context context;
    struct Context *cx = &context;

    context_init(cx, ds, coinStack);

    for (;;) {
        switch (cx->state) {
            case STATE_START_ORDERING_TICKETS:
                state_start_ordering_tickets(cx);
                break;

            case STATE_ORDERING_TICKETS:
                state_ordering_tickets(cx);
                break;

            case STATE_START_PAYING_MONEY:
                state_start_paying_money(cx);
                break;

            case STATE_PAYING_MONEY:
                state_paying_money(cx);
                break;

            case STATE_RETURNING_MONEY:
                state_returning_money(cx);
                break;

            case STATE_PRINTING_TICKETS:
                state_printing_tickets(cx);
                break;

            default:
                fprintf(stderr, "Invalid state\n");
                exit(EXIT_FAILURE);
        }
    }
}




